﻿using System;
using System.Collections.Generic;
using System.IO;

using System.Windows;
using System.Windows.Controls;
using WpfTreeView.Directory.ViewModels;
using Path = System.IO.Path;

namespace WpfTreeView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            var d = new DirectoryStructureViewModel();
            var item1 = d.Items[0];

            d.Items[0].ExpandCommand.Execute(null);
        }

        #endregion
    }
}