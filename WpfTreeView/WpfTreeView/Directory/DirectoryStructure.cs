﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Documents;

namespace WpfTreeView
{
    /// <summary>
    /// A helper class to query information about directories
    /// </summary>
    public static class DirectoryStructure
    {
        /// <summary>
        /// Gets all logical drives on the computer.
        /// </summary>
        /// <returns></returns>

        public static List<DirectoryItem> LogicalDrives()
        {
            //Get every locigcal drive on the machine.
            return DirectoryStructure.GetLogicalDrives().Select(drive => new DirectoryItem() { FullPath = drive, Type = DirectoryItemType.Drive }).ToList();
        }

        internal static object GetLogicalDrives()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the directories top-level content
        /// </summary>
        /// <param name="fullPath">The full path to the directory</param>
        /// <returns></returns>
        public static List<DirectoryItem> GetDirectoryContents(string fullPath)
        {
            //Create empty list
            var items = new List<DirectoryItem>();

            #region Get Folders

            //Try and get directories from the folder.
            //Ignoring any issues doing so.
            try
            {
                var dirs = Directory.GetDirectories(fullPath);

                if (dirs.Length > 0)
                    items.AddRange(dirs.Select(dir => new DirectoryItem { FullPath = dir, Type = DirectoryItemType.Folder }));
            }
            catch
            {
            }

            #endregion

            #region Get files

            //Try and get files from the folder.
            //Ignoring any issues doing so.
            try
            {
                var fs = Directory.GetFiles(fullPath);

                if (fs.Length > 0)
                    items.AddRange(fs.Select(file => new DirectoryItem { FullPath = file, Type = DirectoryItemType.File }));
            }
            catch
            {
            }

            #endregion

            return items;
        }

        #region helpers

        ///<summary>
        ///Find the file or folder name from a full path.
        /// </summary>
        ///<param name="path">The full path</param>
        ///<returns></returns>

        public static string GetFileFolderName(string path)
        {
            //C:\Something\a folder
            //C:\Something\a file
            //a file file.png

            //If we have no path return empty.
            if (string.IsNullOrEmpty(path))
                return string.Empty;

            //Make all slashes back slashes
            var normalizedPath = path.Replace('/', '\\');

            //Find the last backslash in the path
            var lastIndex = normalizedPath.LastIndexOf('\\');

            //If we dont find a backslash, return the path itself.
            if (lastIndex <= 0)
                return path;

            //return the name after the last backslash.
            return path.Substring(lastIndex + 1);
        }

        #endregion
    }
}