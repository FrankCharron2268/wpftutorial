﻿using System.ComponentModel;
using PropertyChanged;

namespace WpfTreeView
{
    /// <summary>
    /// A base view model that Fires Property changed events as needed.
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public class BaseViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// The event that is fired when any child property changes it's value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };
    }
}