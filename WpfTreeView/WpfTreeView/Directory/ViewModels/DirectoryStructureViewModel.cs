﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace WpfTreeView.Directory.ViewModels
{
    /// <summary>
    /// The view model for the applications main Directory View
    /// </summary>
    internal class DirectoryStructureViewModel
    {
        #region Public Properties

        /// <summary>
        /// A list of all directories on the machine
        /// </summary>
        public ObservableCollection<DirectoryItemViewModel> Items { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public DirectoryStructureViewModel()
        {
            //Get the logical drives
            var children = DirectoryStructure.GetLogicalDrives();

            //Create the view models from the data.
            this.Items = new ObservableCollection<DirectoryItemViewModel>(
                children.Select(drive => new DirectoryItemViewModel(drive.FullPath, DirectoryItemType.Drive)));
        }

        #endregion
    }
}