﻿namespace WpfTreeView
{
    /// <summary>
    /// Information about a directory item such as drive, a file or folder.
    /// </summary>
    public class DirectoryItem
    {
        /// <summary>
        /// The type of this item.
        /// </summary>
        public DirectoryItemType Type { get; set; }

        /// <summary>
        /// The absoluyte path to this item.
        /// </summary>
        public string FullPath { get; set; }

        /// <summary>
        /// The name of this directory item.
        /// </summary>
        public string name
        {
            get { return this.Type == DirectoryItemType.Drive ? this.FullPath : DirectoryStructure.GetFileFolderName(this.FullPath); }
        }
    }
}